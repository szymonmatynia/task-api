import { ApiProperty } from '@nestjs/swagger';

export class CreateTaskDTO {
  @ApiProperty({
    example: '...',
    description: 'New title for a brand new task',
  })
  title: string;

  @ApiProperty({
    example: 'Long description... bla bla.',
    description: 'New description for a brand new task',
  })
  description?: string;
}
