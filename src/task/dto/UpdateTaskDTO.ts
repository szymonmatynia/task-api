import { ApiProperty } from '@nestjs/swagger';

export class UpdateTaskDTO {
  @ApiProperty({ example: 'A task title', description: 'New title for a task' })
  title: string;

  @ApiProperty({
    example: 'A task description',
    description: 'New description for a task',
  })
  description?: string;
}
