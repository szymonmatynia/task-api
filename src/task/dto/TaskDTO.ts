import { IsInt, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class TaskDTO {
  @IsInt()
  @ApiProperty({ example: 1, description: 'Task id' })
  id: number;

  @IsString()
  @ApiProperty({ example: 'Wash the dishes', description: 'Task title' })
  title: string;

  @IsString()
  @ApiProperty({ example: 'Long description', description: 'Task description' })
  description?: string;
}
