import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './task.entity';
import { Repository } from 'typeorm';
import { CreateTaskDTO } from './dto/CreateTaskDTO';
import { TaskDTO } from './dto/TaskDTO';
import { UpdateTaskDTO } from './dto/UpdateTaskDTO';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task) private taskRepository: Repository<Task>,
  ) {}

  public async createOne(createTaskDTO: CreateTaskDTO): Promise<TaskDTO> {
    const task: Task = new Task();
    task.title = createTaskDTO.title;
    task.description = createTaskDTO.description;

    await this.taskRepository.save(task);

    return TaskService.toDTO(task);
  }

  public async findOne(taskId: number): Promise<TaskDTO> {
    const task = await this.taskRepository.findOne(taskId);

    if (!task)
      throw new NotFoundException(`Task with the id ${taskId} doesnt exist`);

    return TaskService.toDTO(task);
  }

  public async findAll(): Promise<TaskDTO[]> {
    return TaskService.toDTOCollection(await this.taskRepository.find());
  }

  public async removeOne(taskId: number) {
    const task: TaskDTO = await this.findOne(taskId);

    if (!task) {
      throw new NotFoundException(`Task with the id ${taskId} doesnt exist`);
    }
    await this.taskRepository.remove(task);
  }

  public async updateOne(
    taskId: number,
    updateTaskDTO: UpdateTaskDTO,
  ): Promise<TaskDTO> {
    const task: TaskDTO = await this.findOne(taskId);

    if (!task) {
      throw new NotFoundException(`Task with the id ${taskId} doesnt exist`);
    }

    task.title = updateTaskDTO.title;
    task.description = updateTaskDTO.description;

    await this.taskRepository.save(task);

    return TaskService.toDTO(task);
  }

  // todo this could be generic method for every service :)
  private static toDTO(task: Task): TaskDTO {
    const taskDTO = new TaskDTO();
    taskDTO.id = task.id;
    taskDTO.title = task.title;
    taskDTO.description = task.description;

    return taskDTO;
  }

  private static toDTOCollection(tasks: Task[]): TaskDTO[] {
    return tasks.map((task: Task) => TaskService.toDTO(task));
  }
}
