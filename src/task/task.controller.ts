import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { TaskService } from './task.service';
import { CreateTaskDTO } from './dto/CreateTaskDTO';
import { UpdateTaskDTO } from './dto/UpdateTaskDTO';
import { TaskDTO } from './dto/TaskDTO';
import { ApiResponse } from '@nestjs/swagger';

@Controller('tasks')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Post()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Return just created task',
    type: TaskDTO,
  })
  public async createOne(
    @Body() createTaskRequest: CreateTaskDTO,
  ): Promise<TaskDTO> {
    return this.taskService.createOne(createTaskRequest);
  }

  @Get()
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Returns all tasks',
    type: TaskDTO,
    isArray: true,
  })
  public async findAll(): Promise<TaskDTO[]> {
    return this.taskService.findAll();
  }

  @Get('/:id')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Get specified id task',
    type: TaskDTO,
  })
  public async findOne(@Param('id') taskId: number): Promise<TaskDTO> {
    return this.taskService.findOne(taskId);
  }

  @Put('/:id')
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'Updates specified task',
    type: TaskDTO,
  })
  public async updateOne(
    @Param('id') taskId: number,
    @Body() updateTaskDTO: UpdateTaskDTO,
  ) {
    return this.taskService.updateOne(taskId, updateTaskDTO);
  }

  @Delete('/:id')
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'deletes specified task',
  })
  public async removeOne(@Param('id') taskId: number) {
    return this.taskService.removeOne(taskId);
  }
}
